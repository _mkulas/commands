<?php

namespace Mkulas\Commands;

use Illuminate\Support\ServiceProvider;
use Mkulas\Commands\Services\Contracts\ServiceCommandServiceContract;
use Mkulas\Commands\Services\ServiceCommandService;

class CommandProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->registerServiceContracts();
    }

    public function boot(): void
    {
    }

    private function registerServiceContracts(): void
    {
        $this->app->singleton(ServiceCommandServiceContract::class,ServiceCommandService::class);
    }

}
