<?php

namespace Mkulas\Commands\Commands;

use Illuminate\Console\Command;

class ServiceCommand extends Command
{
    protected $signature = 'make:service';

    protected $description = 'Create service';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        return 0;
    }
}
